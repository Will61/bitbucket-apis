# Bitbucket!
[A VCS hosting website](https://bitbucket.org/)

## API examples?
Api examples based off the reference documentation [https://developer.atlassian.com/bitbucket/api/2/reference/resource/](https://developer.atlassian.com/bitbucket/api/2/reference/resource/).
### Users
Example curl to get user information

`curl -i -u will61:<PASS> -H "Content-Type: application/json" https://api.bitbucket.org/2.0/user`

### Teams
Example curl to get teams which I'm an admin of

`curl -i -u will61:<PASS> -H "Content-Type: application/json" https://api.bitbucket.org/2.0/teams?role=admin`

## GET - Repositories
Example curl commands to get repository information
### Generic
`curl -i -u <USER>:<PASSWORD> -H "Content-Type: application/json" https://api.bitbucket.org/2.0/repositories/<USER_REPOSITORIES>`
### All of my repositories
`curl -i -u will61:<PASSWORD> -H "Content-Type: application/json" https://api.bitbucket.org/2.0/repositories/Will61`
### All repositories of the 'team' user willb611
`curl -i -u will61:<PASSWORD> -H "Content-Type: application/json" https://api.bitbucket.org/2.0/repositories/Willb611`

## POST - Create a repository

`curl -i -X POST -u <USER>:<PASSWORD> -H "Content-Type: application/json" -d '{"scm":"git"}' https://api.bitbucket.org/2.0/repositories/will61/bitbucket-apis`


